<?php

namespace Foo;

class ClassicTicTacToe implements TicTacToeInterface {

   private $board = [
       [NULL, NULL, NULL],
       [NULL, NULL, NULL],
       [NULL, NULL, NULL]
   ];
   private static $x = 'X';
   private static $o = 'O';
   private $winner = FALSE;
   private $lastMove = NULL;

   public function getWinner() {
       return $this->winner;
   }

   public function isEnded() {
       //check row
       for ($i = 0; $i < 3; $i ++) {
           if ($this->board[$i][0] !== NULL && $this->board[$i][0] === $this->board[$i][1] && $this->board[$i][1] === $this->board[$i][2]) {
               $this->winner = $this->board[$i][0];
               return TRUE;
           }
       }

       //check col
       for ($i = 0; $i < 3; $i ++) {
           if ($this->board[0][$i] !== NULL && $this->board[0][$i] === $this->board[1][$i] && $this->board[1][$i] === $this->board[2][$i]) {
               $this->winner = $this->board[0][$i];
               return TRUE;
           }
       }

       //check diagonal 1
       if ($this->board[0][0] !== NULL && $this->board[0][0] === $this->board[1][1] && $this->board[1][1] === $this->board[2][2]) {
           $this->winner = $this->board[0][0];
           return TRUE;
       }
       //check diagonal 2
       if ($this->board[0][2] !== NULL && $this->board[0][2] === $this->board[1][1] && $this->board[1][1] === $this->board[2][0]) {
           $this->winner = $this->board[0][2];
           return TRUE;
       }

       return $this->isTied();
   }

   public function isTied() {
       for ($i = 0; $i < 3; $i ++) {
           for ($j = 0; $j < 3; $j ++) {
               if ($this->board[$i][$j] === NULL) {
                   return FALSE;
               }
           }
       }

       if ($this->winner !== FALSE) {
           return FALSE;
       }

       return TRUE;
   }

   public function putO($x, $y) {
       if ($this->lastMove === NULL || $this->lastMove === self::$x) {
           if ($this->board[$x][$y] === null) {
               $this->board[$x][$y] = self::$o;
               $this->lastMove = self::$o;
           } else {
               throw new FieldTakenException;
           }
       } else {
           throw new WrongPlayerException;
       }
   }

   public function putX($x, $y) {
       if ($this->lastMove === NULL || $this->lastMove === self::$o) {
           if ($this->board[$x][$y] === null) {
               $this->board[$x][$y] = self::$x;
               $this->lastMove = self::$x;
           } else {
               throw new FieldTakenException;
           }
       } else {
           throw new WrongPlayerException;
       }
   }
}